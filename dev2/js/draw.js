'use strict';

const body = document.getElementsByTagName('body')[0],
  canvas = document.getElementById('draw'),
  ctx = canvas.getContext('2d');
let x = null,
  y = null;
let hue = 50,
  brushRadius = 100;
let youCanDraw = false,
  didNotGoBeyond = true,
  isShiftKeyPressed = false,
  intention = false;

const setHue = (hue, flag) => {
  if (flag) {
    return hue === 0 ? 359 : hue - 1;
  }
  return hue === 359 ? 0 : hue + 1;
};

const setRadius = (brushRadius, flag) => {
  if (flag) {
    if (brushRadius < 100) {
      return brushRadius + 1;
    }
    else {
      intention = false;
      return brushRadius - 1;
    }
  }
  else {
    if (brushRadius > 5) {
      return brushRadius - 1;
    }
    else {
      intention = true;
      return brushRadius + 1;
    }
  }
};

document.addEventListener('DOMContentLoaded', onLoad);
window.addEventListener('resize', onResize);
document.addEventListener('keydown', evt => {
  if (evt.key === 'Shift') {
    isShiftKeyPressed = true;
  }

});
document.addEventListener('keyup', evt => {
  if (evt.key === 'Shift') {
    isShiftKeyPressed = false;
  }

});
document.addEventListener('dblclick', evt => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
});

document.addEventListener('mouseup', evt => {
  youCanDraw = false;
  didNotGoBeyond = true;
});
document.addEventListener('mousedown', evt => {
  console.log(`mousedown: ${evt}`);
  youCanDraw = true;
  x = evt.clientX;
  y = evt.clientY;
});
canvas.addEventListener('mouseleave', evt => {
  didNotGoBeyond = false;
  console.log(`mouseleave: ${evt}, didNotGoBeyond = ${didNotGoBeyond}`);
});

document.addEventListener('mousemove', evt => {
  if (youCanDraw && didNotGoBeyond) {
    console.log(`youCanDraw = ${youCanDraw} && didNotGoBeyond = ${didNotGoBeyond}`);
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.lineWidth = brushRadius;
    ctx.lineTo(evt.clientX, evt.clientY);
    console.log(evt.clientX, evt.clientY);
    ctx.stroke();
    x = evt.clientX;
    y = evt.clientY;
    ctx.closePath();
  }
});

function onResize(evt) {
  console.log(evt);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  canvas.width = document.documentElement.clientWidth;
  canvas.height = document.documentElement.clientHeight;
  console.log(canvas.width, canvas.height);
}

function onLoad() {
  body.overflowX = 'hidden';
  canvas.width = document.documentElement.clientWidth;
  canvas.height = document.documentElement.clientHeight;
}

function tick() {
  hue = setHue(hue, isShiftKeyPressed);
  brushRadius = setRadius(brushRadius, intention);
  window.requestAnimationFrame(tick);
}

tick();